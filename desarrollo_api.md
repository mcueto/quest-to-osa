### 2 Desarrollo de API ###
Si alguna vez has utilizado la API de Facebook, sabrás lo común que "deprecaban" su API. Según usted:

* Explique la razón por la cuál esta no es una buena práctica.
* ¿Por qué se dice que las interfaces deben ser "tontas"?
* Como aplicaria usted los conceptos de Encapsulamiento, Flexibilidad, Seguridad y Versionamiento en el desarrollo de una Interfaz?


**RESPUESTAS**:
* No es una buena práctica debido a que al *deprecar* su API de manera muy seguida lo que haces es obligar a los desarrolladores que utilizan tu API a volver a aprender a utilizarla cada vez, los obligas a estar atentos a los cambios y si no tienes una base de desarrolladores muy amplia, ésto podria acabar en la practica con el entusiasmo de éstos desarrolladores, que buscarian alguna alternativa que les *diera menos dolores de cabeza*, en el caso de facebook ellos se aprovechan de que son una empresa grande y que su API es un *must have* para cualquiera que desarrolle portales web.
* Se dicen que deben ser tontas debido a que una interfaz no debe contener código alguno en sus métodos o funciones, ya que la implementación de éstos métodos variará en cada uno de los objetos que la implementen, de ésta manera, la interfaz es *tonta* debido a que no tiene contenido - o no hace nada por si misma -.
* No lo sé, debido a que no suelo trabajar con interfaces, así que tendria que trabajar más con interfaces para poder responder a algo así de manera concreta.
