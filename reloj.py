#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from datetime import timedelta, datetime

class Reloj(object):
    basetime = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    newtime = None

    def __init__(self, hora, minuto):
        self.newtime = self.basetime + timedelta(hours=hora, minutes=minuto)

    def suma(self, minuto):
        self.newtime += timedelta(minutes=minuto)
        return self

    def resta(self, minuto):
        self.newtime -= timedelta(minutes=minuto)
        return self

    def __str__(self):
        return self.newtime.strftime("%H:%M")

def main():
    print(Reloj(8,0))
    print(Reloj(72,8640))
    print(Reloj(-1,15))
    print(Reloj(-25,-160))
    print(Reloj(10,3).suma(-70))

    print(Reloj(0,0))
    print(Reloj(48,30))
    print(Reloj(-23,50))

if __name__ == '__main__':
    main()
