### 1 Desarrollo ###

Implementar un reloj que sea capaz de manejar horas sin fechas. Debe ser capaz de agregarle y restarle minutos.
Como input recibirá dos integer (hora, minutos). Retornará un string con la hora que representa. Se toma como base las '00:00'.

Al desarrollar, crear casos de prueba.

Ejemplo de Pruebas

input-> Output:

==========================
  PRUEBAS OSA

 * Reloj(8,0) -> '08:00'
 * Reloj(72,8640) -> '00:00'
 * Reloj(-1,15) -> '23:15'
 * Reloj(-25,-160) -> '20:20'
 * Reloj(10,3).suma(-70) -> '08:53'

==========================

==========================
  PRUEBAS MARCELO CUETO

 * Reloj(0,0) -> '00:00'
 * Reloj(48,30) -> '00:30'
 * Reloj(-23,50) -> '01:50'

==========================
