### 3 Comprensión ###

Con el siguiente código en python:


    # -*- coding: utf-8 -*-
    def z(c):
        if c[-1] == '?':
    	    return 3
    	else:
    	    return 2

    def y(b):
        r = [l.isupper() for l in b[:-1] if l.isalpha()]

        if r and reduce(lambda a,x : a and x, r): # !
            return 4
        else:
            return z(b)


    def x(a):
        a = ''.join(a.strip().split(' '))
        return y(a) if a else 1


Llamadas:

* x('')
* x('1 2 3 4')
* x('1 2 3 4 A?')
* x('hola?')
* x('HOLA')


Explique según usted:

* ¿Qué hace este código?
* ¿En la línea marcada con un # !: ¿Qué problema se puede presentar? ¿como se soluciona?
* ¿Qué mejora haría usted en la implementación del código?

**RESPUESTAS:**

* NI, IDEA.
* Se me presentó el problema de *name 'reduce' is not defined* debido a que ahora esa función se encuentra en functools, así que se soluciona importando *reduce* desde ese modulo.
  Otro problema se encuentra en que la variable a no se encuentra definida,

* Primero que todo cambiaria los nombres de las funciones y sus atributos para hacerlas más autoexplicativas, segundo agregaria comentarios y crearía un archivo markdown explicando como utilizar esa función(en el caso de no quedar tan autoexplicativa)
