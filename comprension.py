# -*- coding: utf-8 -*-
from functools import reduce

def z(c):
    # Si el último carácter es un "?", retorna 3, sino, retorna 2
    if c[-1] == '?':
        return 3
    else:
        return 2

def y(b):
    # Asigna a r tantos True como Mayusculas haya en una cadena alfabetica
    r = [l.isupper() for l in b[:-1] if l.isalpha()]

    if r and reduce(lambda a,x : a and x, r): # !
        return 4
    else:
        return z(b)


def x(a):
    a = ''.join(a.strip().split(' '))
    return y(a) if a else 1

def main():
    print(x(''))
    print(x('1 2 3 4'))
    print(x('1 2 3 4 A?'))
    print(x('hola?'))
    print(x('HOLA'))

if __name__ == '__main__':
    main()
